#+TITLE: jabber.el
#+SUBTITLE: XMPP client for Emacs
#+DESCRIPTION: Documentation
#+startup: inlineimages

* Explanation
:PROPERTIES:
:CUSTOM_ID: explanation
:END:
This is jabber.el 0.8.92, an XMPP client for Emacs.  XMPP (also known as 'Jabber') is an instant messaging system; see http://xmpp.org for more information.

New resources
+ [[https://codeberg.org/emacs-jabber/emacs-jabber/][project page]]
+ chat: [[xmpp:emacs@salas.suchat.org?join][xmpp:emacs@salas.suchat.org?join]] (general Emacs and jabber.el talk)
+ chat: xmpp:jabber-el@conference.hmm.st?join (jabber.el developement)

Old resources
+ [[http://sourceforge.net/projects/emacs-jabber][project page]]
+ [[http://emacs-jabber.sourceforge.net][home page]]
+ [[http://www.emacswiki.org/cgi-bin/wiki/JabberEl][wiki page]]
+ mailing list:
  * http://lists.sourceforge.net/lists/listinfo/emacs-jabber-general
  * http://dir.gmane.org/gmane.emacs.jabber.general
+ chat: jabber.el@conference.jabber.se and emacs@conference.jabber.ru (Russian, English)

** Differences from [[https://github.com/legoscia/emacs-jabber][upstream]]
:PROPERTIES:
:CUSTOM_ID: upstream-differences
:END:
1. (WIP) Support for [[https://xmpp.org/extensions/xep-0363.html][XEP-0363: HTTP File Upload]]
2. Support for [[https://xmpp.org/extensions/xep-0280.html][XEP-0280: Message Carbons]]
3. Use of the deprecated =cl= library is almost completely replaced with =cl-lib=
4. Build system changed from Autotools to a =Makefile=
5. Lots of cleanup by addressing =checkdoc= and byte-compiler warnings - addition/improvement of docstrings, function/variable declarations, etc
6. Converted to [[#literate-program][an Org literate program]]

** Requirements
:PROPERTIES:
:CUSTOM_ID: requirements
:END:
jabber.el runs on GNU Emacs 27.1 or later.

The file hexrgb.el (http://www.emacswiki.org/emacs/hexrgb.el) is needed for MUC nick coloring feature.  A copy is located in the compat directory, and used if the configure script doesn't find another copy already installed.

** Encrypted connections
:PROPERTIES:
:CUSTOM_ID: encrypted-connections
:END:
Many Jabber servers require encrypted connections, and even if yours doesn't it may be good idea.  To get an encrypted connection, the most convenient option is to use GNU Emacs 24 with GnuTLS support compiled in.  You can check whether you have that by typing:

: M-: (gnutls-available-p)

If that commands shows =t= in the echo area, then you have working GnuTLS support.  If it shows =nil= or signals an error, then you don't.

Failing that, jabber.el will use the starttls.el library, which requires that the GnuTLS command line tool "gnutls-cli" is installed.

In Debian-based distributions, "gnutls-cli" is in the "gnutls-bin" package.

The above applies to STARTTLS connections, the most common way to encrypt a Jabber connection and the only one specified in the standards.  STARTTLS connections start out unencrypted, but switch to encrypted after negotiation.  jabber.el also supports connections that are encrypted from start.  For this it uses the tls.el library, which requires either "gnutls-cli" or the OpenSSL command line tool "openssl" to be installed.

To use the latter form of encryption, customize =jabber-account-list=.

Note that only the connection from you to the server is encrypted; there is no guarantee of connections from your server to your contacts' server being encrypted.

** Usage
:PROPERTIES:
:CUSTOM_ID: usage
:END:
To connect to a Jabber server, type =C-x C-j C-c= (or equivalently =M-x jabber-connect-all=) and enter your JID.  With prefix argument, register a new account.  You can set your JID permanently with =M-x jabber-customize=.

Your roster is displayed in a buffer called *-jabber-*.  To disconnect, type =C-x C-j C-d= or =M-x jabber-disconnect=.

You may want to use the menu bar to execute Jabber commands.  To enable the Jabber menu, type =M-x jabber-menu=.

For a less terse description, read the enclosed manual.

For bug reports, help requests and other feedback, use the trackers and forums at the project page mentioned above.

** Configuration
:PROPERTIES:
:CUSTOM_ID: configuration
:END:
All available configuration options are described in the manual.  This section only serves to point out the most important ones.

To change how you are notified about incoming events, type =M-x customize-group RET jabber-alerts=.

To activate logging of all chats, set =jabber-history-enabled= to =t=.  By default, history will be saved in =~/.jabber_global_message_log=; make sure that this file has appropriate permissions.  Type =M-x customize-group RET jabber-history= for more options.

By default, jabber.el will send a confirmation when messages sent to you are delivered and displayed, and also send "contact is typing" notifications.  To change this, type =M-x customize-group RET jabber-events=, and set the three =jabber-events-confirm-*= variables to nil.

** File transfer
:PROPERTIES:
:CUSTOM_ID: file-transfer
:END:
This release of jabber.el contains support for file transfer.  You may need to configure some variables to make it work; see the manual for details.

** XMPP URIs
:PROPERTIES:
:CUSTOM_ID: xmpp-uris
:END:
It is possible to make various web browsers pass links starting with "xmpp:" to jabber.el.  In the ideal case, this works right after running "make install".  Otherwise, see the manual, section "XMPP URIs".

** Literate program
:PROPERTIES:
:CUSTOM_ID: literate-program
:END:
jabber.el is a literate program made using Org. Thanks to [[https://github.com/melpa/melpa/issues/7408][package.el limitations]], we check in the tangled sources to Git. The tangled sources are what Emacs knows how to load.

Alternatively, it is possible to use =literate-elisp-load-file= from [[https://github.com/jingtaozf/literate-elisp][literate-elisp]] to directly load this Org literate program. This way, links to the source (e.g. =xref=, =describe-*= buffers, byte-compilation messages) take the user directly to the Org file rather than to the tangled source.

If a source block does not have syntax highlighting, press =M-o M-o= (=font-lock-fontify-block=) in it.

*** TODO literate/organizational tasks [37%]
:PROPERTIES:
:CUSTOM_ID: literate-organizational-tasks
:END:
1. [ ] (maybe) make dependencies optional and tangle them to separate files, reducing load time for users.
2. [ ] contemplate [[https://github.com/melpa/melpa/issues/7408][distribution strategies]]
3. [X] make headings for remaining definitions - some FSM-related definitions remain.
4. [ ] move tests to this file (probably migrate them to =ert= or =buttercup= first), in sub-headings of their concerned components.
5. [ ] move dependencies to the Dependencies heading; also make library headers for them
6. [X] move per-file author information and copyright notice here, and delete the empty .el files
   * But it will cease to remain applicable as soon as we move anything around...
7. [ ] "Code" has a lot of direct sub-headings, making it somewhat cumbersome to navigate; someone with a better understanding of the program could organize these better
8. [X] The tangled file currently does not list all the other authors (currently listed in =:COPYRIGHT:= drawers). We could add them all at once in the library headers section...or something else. 🤔

** Debugging tips
:PROPERTIES:
:CUSTOM_ID: debugging-tips
:END:
Useful tips for debugging:

- There is a buffer called ~*fsm-debug*~ that displays all transitions and errors during the event handling.
- There is a =jabber-debug= customization group.
- You can set the [[file:jabber.org::#debug-log-xml][jabber-debug-log-xml]] custom variable to ~t~ to enable the XML debug console.
- The XML console is a buffer called ~*-jabber-console-ACCOUNT-*~ by default. Enable ~jabber-debug-log-xml~ and switch to that buffer to see the incoming and outgoing XML stanzas. See [[file:jabber.org::#xml-console-mode][xml-console-mode]].

** fsm.el - the Finite State Machine library
:PROPERTIES:
:CUSTOM_ID: fsm
:END:
fsm.el implements functions to define multiple [[https://en.wikipedia.org/wiki/Finite-state_machine][finite state machines]] (FSM), their states, and all the events associated to each of them.

The following is a list of the most important functions or macros defined in this library:

- ~(define-state-machine name &key start sleep)~
- ~(define-state fsm-name state-name arglist &body body)~
- ~(define-enter-state fsm-name state-name arglist &body body)~
- ~(define-fsm name &key strat sleep states ...)~
- ~(fsm-send fsm event &optional callback)~
- ~(fsm-call fsm event)~

It is required a name and the starting state to define a new FSM. The ~define-state-machine~ creates a new function called ~start-NAME~. Its ~start~ argument is a function argument and body definition used by the created function. The result of the new function must be a list ~(STATE STATE-DATA [TIMEOUT])~ which is the starting state of the machine.

See [[file:jabber.org::*jabber-connection][jabber-connection]] section for an example. Its ~:start~ parameter explicitly mentioned, and its value is a list with the arguments ( ~(username server resource ...)~ ), a docstring ( ~"Start a jabber connection."~ ) and the body of the ~start-jabber-connection~ function.

The machine requires states. They are defined with the ~define-state~ function.

** The jabber-connection FSM
:PROPERTIES:
:CUSTOM_ID: jabber-connection-fsm
:END:
jabber.el use a finite state machine (FSM) to track the current Jabber connection step. It defines a FSM called [[file:jabber.org::#fsm-connection][jabber-connection]] (or ~jc~ when it is used as parameter in functions) and several states along with their sentinels. The Org-mode tag ~:fsm:~ is used at jabber.org headlines to describe FSM definitions.

*** States
:PROPERTIES:
:CUSTOM_ID: states
:END:
The following graph shows the states and their transitions, as of commit [[https://tildegit.org/wgreenhouse/emacs-jabber/commit/dddcccb926f422b03d22a66b60db46f1266eb141][dddcccb926]] (2021-03-20). The nodes represent the states and the arrows are events.

All states have filter and sentinel events that do not change the FSM state. Also, they have a ~:do-disconnect~ event that change the FSM to the ~nil~ state except for the ~connecting~ state.

Some state changes depend on the event and the data received, in this case, the event name has a number added. For instance, ~:stream-start1~, ~:stream-start2~ and ~:stream-start3~ is the same event (~:stream-start~) but triggers different states changes depending on the data received.

#+name: fig:states
#+BEGIN_SRC dot :file images/states-dot.png :exports results :tangle no
  digraph "jabber-connection" {
          nil;

          connecting -> connected [label=":connected"];
          connecting -> nil [label=":connection-failed"];
          connecting -> defer [label=":do-disconnect"];

          connected -> "connected" [label=":filter, :sentinel, :stream-start1,"];
          connected -> "register-account" [label=":stream-start2, :stanza1"];
          connected -> "legacy-auth" [label=":stream-start3"];
          connected -> "starttls" [label=":stanza2"];
          connected -> "sasl-auth" [label=":stanza3"];

          "register-account" -> "register-account" [label=":stanza"];

          starttls -> connected [label=":stanza"];

          "legacy-auth" -> "legacy-auth" [label=":stanza"];
          "legacy-auth" -> "session-established" [label=":authontication-success"];
          "legacy-auth" -> "nil" [label=":authentication-failure"];

          "sasl-auth" -> "sasl-auth" [label=":stanza"];
          "sasl-auth" -> "legacy-auth" [label=":use-legacy-auth-instead"];
          "sasl-auth" -> bind [label=":authentication-success"];
          "sasl-auth" -> nil [label=":authentication-failure"];

          bind -> bind [label=":stream-start, :stanza1"];
          bind -> nil [label=":stanza2, :bind-failure, :session-failure"];
          bind -> "session-established" [label=":bind-success, :session-success"];

          "session-established" -> "session-established" [label=":stanza; :roster-update, :timeout, :send-if-connected"];
  }
#+END_SRC

#+caption: Implemented states in the Jabber FSM.
#+RESULTS: fig:states
[[file:images/states-dot.png]]

** Stanza processing
:PROPERTIES:
:CUSTOM_ID: stanza-processing
:END:
The following is a brief summary about the stanza processing.

1. The ~:session-established~ state is reached.
2. The FSM receives the event ~:stanza~ at the ~:session-established~ state.
3. If no error has been found, call ~jabber-process-input~. See [[file:jabber.org::*jabber-process-input][jabber-process-input]] section.
4. Select one of the following variables depending on the type of message received: ~jabber-iq-chain~, ~jabber-presence-chain~ and ~jabber-message-chain~. All of them contains a list of functions that process its type of message.
5. Call all of their functions with the jabber connection and XML data as parameters .
6. Continue in the same state.

* Tutorials
:PROPERTIES:
:CUSTOM_ID: tutorials
:END:
** Installation
:PROPERTIES:
:CUSTOM_ID: installation
:END:
*** from MELPA
:PROPERTIES:
:CUSTOM_ID: from-melpa
:END:
1. Add the MELPA repositories to your Emacs - https://melpa.org/#/getting-started
2. Type =M-x package-install RET jabber RET=

If all goes well, =jabber.el= commands like =jabber-connect= should now be available in the =M-x= menu.

*** from source
:PROPERTIES:
:CUSTOM_ID: from-source
:END:
1. Clone the repository by typing the following into a terminal -
   #+BEGIN_SRC shell
   mkdir ~/emacs-jabber/
   cd ~/emacs-jabber/
   git clone https://codeberg.org/emacs-jabber/emacs-jabber
   make
   #+END_SRC

2. Add the following lines to your =init.el= -
   #+BEGIN_SRC emacs-lisp
   (add-to-list 'load-path "~/emacs-jabber/")
   (load "~/emacs-jabber/jabber-autoloads")
   #+END_SRC
   ...and, while still in your =init.el=, type =M-x eval-buffer RET=.

If all goes well, =jabber.el= commands like =jabber-connect= should now be available in the =M-x= menu.

To install the Info documentation, copy =jabber.info= to =/usr/local/info= and run ="install-info /usr/local/info/jabber.info"=.

* How-to guides
:PROPERTIES:
:CUSTOM_ID: how-to-guides
:END:
** How to contribute to jabber.el
:PROPERTIES:
:CUSTOM_ID: how-to-contribute
:END:
1. Fork and clone the repository.
   #+BEGIN_SRC shell :tangle no
     mkdir ~/git/ && cd ~/git/
     git clone https://codeberg.org/YOUR-ACCOUNT/emacs-jabber
   #+END_SRC
   + You can also send patches to [[mailto:wgreenhouse@tilde.club][wgreenhouse@tilde.club]], using [[https://git-send-email.io/][git-send-email]]. In that case, no forking is necessary.

2. Install development dependencies.
   1. Ensure you have Org mode v9.3.8 or higher. If necessary, install from source -
      #+BEGIN_SRC shell :tangle no
        git clone https://code.orgmode.org/bzg/org-mode.git
        cd ~/git/org-mode/
        make
      #+END_SRC
   2. Optionally, evaluate the following to install additional development tools (requires MELPA to be set up as package source) -
      #+BEGIN_SRC emacs-lisp :tangle no
        (mapcar #'package-install
                '(indent-lint package-lint relint nameless literate-elisp))
      #+END_SRC

3. Make your edits to =jabber.org= (not =jabber.el=), and save. Then, tangle the file -
   #+BEGIN_SRC
   M-x compile RET make -Bk RET
   #+END_SRC
   + If you installed Org from source, be sure to mention the =ORG_PATH= -
     #+BEGIN_SRC
     M-x compile RET ORG_PATH=/path/to/org/source/lisp/ make -Bk RET
     #+END_SRC
   + If you don't like working with literate programs, just edit =jabber.el= and we'll incorporate your PR into =jabber.org=.

   This tangles the literate program and byte compiles the tangled sources. Try to address any byte compilation errors.

   You can also run =make dev= to tangle, compile, and run linters in one go, and address their output.

   Stage the changes you made to =jabber.org=, as well as any changes the tangling process made to =jabber.el=.

   Try to follow [[https://cbea.ms/git-commit/#seven-rules][the seven rules of a great Git commit message]] in your commits.

4. Update the documentation.
   1. Add your name to the [[#contributors][list of contributors]].
   2. Document user-facing changes in [[file:CHANGELOG.org][CHANGELOG.org]].
   3. Update or add other documentation in this [[file:README.org][README.org]] if necessary. Try to follow the [[https://diataxis.fr/][Diataxis Framework]].

5. Push and create your PR.

* TODO maintenance [0%]
:PROPERTIES:
:CUSTOM_ID: maintenance
:END:
1. [ ] Satisfy =M-x checkdoc=
2. [ ] Use =rx= where regular expressions get hairy
   * [ ] =jabber-jid-username=
   * [ ] =jabber-jid-server=
   * [ ] =jabber-jid-resource=
3. [ ] hexrgb.el is not available on MELPA
4. [ ] Migrate tests to ERT/Buttercup/etc
5. [ ] Add Windows support to developer tutorial
6. [ ] Create testing for tutorial

* Credits
:PROPERTIES:
:CUSTOM_ID: credits
:END:
** Developers
:PROPERTIES:
:CUSTOM_ID: developers
:END:
+ Tom Berger
+ Magnus Henoch
+ Kirill A. Korinskiy
+ Detlev Zundel
  - wmii support
+ Evgenii Terechkov

** Contributors
:PROPERTIES:
:CUSTOM_ID: contributors
:END:
+ Georg Lehner
  - network transport functions
+ Anthony Chaumas-Pellet
+ Jérémy Compostella
+ Mathias Dahl
  - history logging
  - watch functionality
+ Mario Domenech Goulart
  - sawfish support
  - xmessage support
+ Nolan Eakins
+ Ami Fischman
  - Chat State Notifications
+ François Fleuret
+ David Hansen
+ Adam Sjøgren
  - notifications.el support
+ Rodrigo Lazo
  - notifications.el support
  - libnotify.el support
+ Justin Kirby
+ Carl Henrik Lunde
  - network transport functions
  - activity tracking
+ Olivier Ramonat
+ Andrey Slusar
+ Valery V. Vorotyntsev
  - GMail notifications
+ Milan Zamazal
+ Xavier Maillard
+ Vitaly Mayatskikh
+ Alexander Solovyov
+ Demyan Rogozhin
  - XML console mode
+ Michael Cardell Widerkrantz
  - tmux support
+ Case Duckworth (acdw)
  - [[https://codeberg.org/emacs-jabber/emacs-jabber/pulls/2][PR #2]]

** Maintainers
:PROPERTIES:
:CUSTOM_ID: maintainers
:END:
+ wgreenhouse
  - 2021 resurrection
+ cngimenez
  - HTTP Upload support
  - documentation for FSM and its use in this project
+ contrapunctus
  - literate Org migration
  - Makefile (shoutout to tomasino of #team@irc.tilde.chat for the Makefile-debugging help)
